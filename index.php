<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Blog content">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <title>Blog Title</title>
</head>
<body>
<div class="container">
    <header>
        <div id="logo-menu">Blog Template</div>
        <input type="checkbox" id="btn-menu">
        <label for="btn-menu"><img src="images/menu.jpg" width="50%" height="100%" align="center"></label>
        <div id="banner">
            <div id="banner-left">Blog template</div>
            <div id="banner-right-social">
                <ul>
                    <li><a href="#"><img src="images/social/facebook.png" width="40" height="40"></a> </li>
                    <li><a href="#"><img src="images/social/twitter.png" width="40" height="40"></a> </li>
                    <li><a href="#"><img src="images/social/instagram.png" width="40" height="40"></a> </li>
                </ul>
            </div>
        </div>
        <nav class="menu">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Entertainment</a></li>
                <li><a href="#">Local News</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </nav>
        <!--mobile search view-->
        <form action="" method="post" id="search_bar">
            <input type="text" name="" id="search_text" placeholder="Search">
            <input type="button" name="" id="search_button">
        </form>
    </header>
    <div id="main">
        <div class="content">
            <article>
                <h1>Title here</h1>
                <div id="slider">
                    <img src="images/slide3.png" width="100%" height="45%">
                </div>
                <p>
                    Rót đến tràn ly, anh chìm đắm trong men cay đắng nồng
                    Khóc chát làn mi, uống cùng anh cho đêm nay say chất ngất!
                    Dẫu năm tháng ấy còn đâu những đam mê ta kiếm tìm?
                    Màu mắt xanh ngời lạc giữa mây ngàn về chốn xa xôi
                    Hãy say cùng anh, hãy hát cùng anh, hãy khóc cùng anh
                    Thêm một lần...
                    Để anh được gần trái tim của em dù trong phút giây
                    Hình bóng người tan biến dần phía sau những nỗi sầu

                    Với em chắc quá đủ cho một mối tình
                    Dẫu em không thể ở lại với anh
                    Mình chẳng cùng với nhau đi hết quãng đường, ôm ấp hi vọng một ngày ngát xanh
                    Tháng năm thăng trầm dòng đời ngả nghiêng
                    Mình tự rời bỏ nhau, say đến điên dại, say hết kiếp người, say cho cháy lòng
                    <a href="#"><img src="images/readmore.png" width="70" height="100%"></a>
                </p>
            </article>
            <article>
                <h1>Title here</h1>
                <div id="slider">
                    <img src="images/slide3.png" width="100%" height="45%">
                </div>
                <p>
                    Rót đến tràn ly, anh chìm đắm trong men cay đắng nồng
                    Khóc chát làn mi, uống cùng anh cho đêm nay say chất ngất!
                    Dẫu năm tháng ấy còn đâu những đam mê ta kiếm tìm?
                    Màu mắt xanh ngời lạc giữa mây ngàn về chốn xa xôi
                    Hãy say cùng anh, hãy hát cùng anh, hãy khóc cùng anh
                    Thêm một lần...
                    Để anh được gần trái tim của em dù trong phút giây
                    Hình bóng người tan biến dần phía sau những nỗi sầu

                    Với em chắc quá đủ cho một mối tình
                    Dẫu em không thể ở lại với anh
                    Mình chẳng cùng với nhau đi hết quãng đường, ôm ấp hi vọng một ngày ngát xanh
                    Tháng năm thăng trầm dòng đời ngả nghiêng
                    Mình tự rời bỏ nhau, say đến điên dại, say hết kiếp người, say cho cháy lòng
                    <a href="#"><img src="images/readmore.png" width="70" height="100%"></a>
                </p>
            </article>
        </div>
        <!-- slidebar widget area goes here-->
        <div class="side">
            <form action="" method="">
                <input type="text" name="" id="search_text" placeholder="Search">
                <input type="button" name="" id="search_button">
            </form>
        </div>
        <div class="side">
            <div id="head-tab"></div>
            Copyright Free, Feel free to use this template for any purpose
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#btn-menu').on('click',function () {
        $('.menu').toggleClass('show')
    });
</script>
</body>
</html>